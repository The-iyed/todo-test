"use strict";

const getTodosFromLocalStorage = (key = "pending") => {
  const todos = localStorage.getItem(key);
  if (todos) {
    return JSON.parse(todos);
  }
  localStorage.setItem(key, "[]");
  return [];
};

const setTodosFromLocalStorage = (element, key = "pending") => {
  let todos = getTodosFromLocalStorage();
  todos.push(element);
  const stringTodos = JSON.stringify(todos);
  localStorage.setItem(key, stringTodos);
};





const updateBtn = document.querySelector(".btn_update");
const addBtn = document.querySelector(".container-btn-submit");
const ContainerNode = document.querySelector(".container-tasks");
const pending = getTodosFromLocalStorage("pending");
const completed = getTodosFromLocalStorage("completed");
let inputValue = document.querySelector(".container-input");
let information = document.querySelector(".info");
let ranId = 0;
let tasks = [];
let finished = [];
const remArray = [];
const upArrray = [];
const downArray = [];


function removeFromArr1Arr2(arr1,arr2){
  // remove from arr1 item in arr2
  for (let i = 0; i < arr2.length; i++) {
     if ( arr1.includes(arr2[i])){
       arr1.splice(arr1.indexOf(arr2[i]),1);
     }
    
  }return arr1
}

 const genratePending = (tasks) =>{
  const containerPending = document.querySelector(".container-tasks-pending");
  containerPending.textContent="";
  const newTasks = getTodosFromLocalStorage("pending");
   newTasks.forEach(el => {
    const html =`
    <div class="task" style="background-color: rgb(${randomColor()}, ${randomColor()}, ${randomColor()});min-width: 360px;"">
      <div>
          <p class="conatiner-task " >${el}</p>
     </div>  
    
    </div>`;
    containerPending.insertAdjacentHTML("afterbegin", html);
   })
  } 

const generateInfo = () =>{
  const newCompleted = getTodosFromLocalStorage ("completed");
  const sotragePending = getTodosFromLocalStorage("pending");
  let information = document.querySelector(".info");
  information.textContent = "you have task  ";
  document.querySelector(".container-completed--info").textContent="and   task";
  let index = information.textContent.indexOf("ve");
  let index2 = document.querySelector(".container-completed--info").textContent.indexOf("d ") ;
  console.log(index2);

  document.querySelector(".info").textContent = information.textContent.replace(information.textContent[index+ 3],sotragePending.length+" t");
  document.querySelector(".container-completed--info").textContent = document.querySelector(".container-completed--info").textContent.replace(information.textContent[index2+1]," "+newCompleted.length+" ");

}


const genratecomPleted = () => {
  const containerPending = document.querySelector(".container-tasks-pending");
  containerPending.textContent="";
  const newFinished =getTodosFromLocalStorage ("completed")
   newFinished.forEach(el => {
    const html =`
    <div class="task" style="background-color: rgb(${randomColor()}, ${randomColor()}, ${randomColor()});">
    <div>
        <p class="conatiner-task underlined" id="${ranId}">${el}</p>
    </div>  
    
   </div>`;
    containerPending.insertAdjacentHTML("afterbegin", html);
   })
   
}
const removeFromArrayWord = (Arr,Word)=>{
  let i ;
  for (i=0 ; i < Arr.length; i++){
    if (Arr[i]===Word){
        Arr.splice(i,1);
      return Arr
    }
  }
  return Arr
}

const changeName = (Arr , word , newWord) => {
  let i ; 
  for (i = 0 ; i < Arr.length ; i++){
    if (Arr[i] === word){
      Arr[i]=newWord
      return Arr
    }
  }
  return Arr
}

const render = () => {
  pending.forEach((task) => {
    tasks.push(task);
    const html = taskHTML1(randomColor, ranId, task);
    ContainerNode.insertAdjacentHTML("afterbegin", html);
    ranId++;
    removeTask () ;

    updateTask(upArrray);
    const pending = getTodosFromLocalStorage("pending");    
  });


};

window.onload = () => {
  render();
  genratePending (tasks);
  generateInfo ()

  const newFinished =getTodosFromLocalStorage("completed");

   newFinished.forEach(el => {
    const htmlC =`
    <div class="task" style="background-color: rgb(${randomColor()}, ${randomColor()}, ${randomColor()});min-width: 360px;"">
      <div>
          <p class="conatiner-task underlined" id="${ranId}">${el}</p>
     </div>  
     <div class="action">
          <span class="task-update"><img src="./pencil-square.svg" class="${ranId} update" alt="update"></span>
          <span class="task-remove"><img src="./trash-fill.svg" class="${ranId} remove" alt="remove"></span>
      </div>
    </div>`;
    ranId++;
    ContainerNode.insertAdjacentHTML("afterbegin", htmlC);
    removeTask () ;
    updateTask(upArrray);
   })


};

const taskHTML1 = (randomColor, ranId, inputValue) => `
<div class="task" style="background-color: rgb(${randomColor()}, ${randomColor()}, ${randomColor()});">
    <div>
        <p class="conatiner-task" id="${ranId}">${inputValue}</p>
    </div>  
    <div class="action">
        <span class="task-update"><img src="./pencil-square.svg" class="${ranId} update" alt="update"></span>
        <span class="task-remove"><img src="./trash-fill.svg" class="${ranId}d remove" alt="remove"></span>
    </div>
</div>`;
const randomColor = () => {
  let result = Math.random() * (225 - 0) + 0;
  return Math.floor(result);
};

const addTask = (inputValue, ranId, randomColor, ContainerNode) => {
  const info = document.querySelector(".info");
  
  
  if (inputValue.value != "") {
    inputValue.value = inputValue.value.replace(
      inputValue.value[0],
      inputValue.value[0].toUpperCase(),
    );
    const html =`
    <div class="task" style="background-color: rgb(${randomColor()}, ${randomColor()}, ${randomColor()});">
      <div>
          <p class="conatiner-task" id="${ranId}">${inputValue.value}</p>
     </div>  
     <div class="action">
          <span class="task-update"><img src="./pencil-square.svg" class="${ranId} update" alt="update"></span>
          <span class="task-remove"><img src="./trash-fill.svg" class="${ranId}d remove" alt="remove"></span>
      </div>
    </div>`;
    tasks.push(inputValue.value);
    
    setTodosFromLocalStorage(inputValue.value, "pending");
    const pending = getTodosFromLocalStorage("pending");
    ContainerNode.insertAdjacentHTML("afterbegin", html);
    /**Render todos  */
    inputValue.value = "";   
    genratePending  (tasks);
    generateInfo ()


  }
  ranId++;
  
};

const removeTask = () => {
  const info = document.querySelector(".info");
  const removeBtn = document.querySelectorAll(".remove");
  removeBtn.forEach((el) => {
    el.addEventListener("click", function () {
      let i;
      let j;
      let str =  document.getElementById(String((el.className[0])[0]));
   
      const todoP=getTodosFromLocalStorage("pending");
      const todoC=getTodosFromLocalStorage("completed");
      const todoCo = removeFromArrayWord(todoC,str.textContent) ;
      const todoPo = removeFromArrayWord(todoP,str.textContent) ;  
      localStorage.setItem("pending",JSON.stringify( todoPo));
      localStorage.setItem("completed",JSON.stringify(todoCo));
      el.parentElement.parentElement.parentElement.remove();
      // TODO: Update pending info
      generateInfo ()

    }); 
  })};

const updateTask = (upArrray) => {
  const tasks = document.querySelectorAll(".update");
  tasks.forEach((task) => {
    task.addEventListener("click", function () {
      // get the old note name
      let noteName = document.getElementById(String(task.className[0]));
      upArrray.push(String(task.className[0]));
      const editField = document.querySelector(".edit-field");
      const containerInputField = document.querySelector(
        ".container-input-field"
      );
      const taskContainer = document.querySelector(".container-tasks");
      const infoContainer = document.querySelector(".container-info");
      editField.classList.remove("hide");
      containerInputField.classList.add("hide");
      taskContainer.classList.add("hide");
      infoContainer.classList.add("hide");
   
      //migrate the old value to the new update field
      document.querySelector(".new--note").value = noteName.textContent;
      generateInfo ()

    });
  });
};




addBtn.addEventListener("click", function () {
  main();

});

inputValue.addEventListener("keypress", function (event) {
  if (event.key === "Enter") {
    main();
  }
});

const main = () => {
  addTask(inputValue, ranId, randomColor, ContainerNode);
  removeTask();
  updateTask(upArrray);
  generateInfo ()


};

updateBtn.addEventListener("click", function () {
  let noteName = document.getElementById(upArrray[upArrray.length - 1]);
  noteName.textContent = document.querySelector(".new--note").value;
  
  if(document.querySelector(".selection").value === "completed"){
        finished.push(document.querySelector(".new--note").value);
        const newCompleted = getTodosFromLocalStorage ("completed");
        const sotragePending = getTodosFromLocalStorage("pending");
        newCompleted.push(document.querySelector(".new--note").value); // push the new value to the completed array 
        const newPending = removeFromArrayWord (sotragePending, noteName.textContent);
        const newCompletedV1 = removeFromArrayWord (newCompleted , noteName.textContent);
        localStorage.setItem("completed",JSON.stringify (newCompletedV1));
        localStorage.removeItem("pending");
        localStorage.setItem("pending",JSON.stringify(newPending));
        noteName.classList.add("underlined"); 


  }
  if(document.querySelector(".selection").value === "pending"){  
    const note =noteName.textContent
    noteName.classList.remove("underlined")
    let todoP=getTodosFromLocalStorage("pending");
    let todoC=getTodosFromLocalStorage("completed"); //get completeed
    todoP.push(note);
    const stringPending =removeFromArrayWord(todoP,note); // remove the old value from the array 
    const stringTodos = removeFromArrayWord(todoC,note) ;  // remove the old value from the array
    // remove task from completed
    stringPending.push(document.querySelector(".new--note").value); // push the new vale the Pending
    
    localStorage.setItem("pending",JSON.stringify(stringPending)); // set the new pending
    localStorage.setItem("completed",JSON.stringify(stringTodos)); ///set the new completed

}
  generateInfo ();
  let editField = document.querySelector(".edit-field");
  let containerInputField = document.querySelector(".container-input-field");
  let containerTask = document.querySelector(".container-tasks");
  let containerInfo = document.querySelector(".container-info");
  editField.classList.add("hide");
  containerInputField.classList.remove("hide");
  containerTask.classList.remove("hide");
  containerInfo.classList.remove("hide");
});

document.querySelector(".btn-cancel").addEventListener("click", function () {
  cleanInputs();
});

const cleanInputs = () => {
  let editField = document.querySelector(".edit-field");
  let containerInputField = document.querySelector(".container-input-field");
  let containerTask = document.querySelector(".container-tasks");
  let containerInfo = document.querySelector(".container-info");

  editField.classList.add("hide");
  containerInputField.classList.remove("hide");
  containerTask.classList.remove("hide");
  containerInfo.classList.remove("hide");
};


const displayPending = document.querySelector(".pop-pends");
displayPending.addEventListener("click",function(){
  genratePending (tasks);
  document.querySelector(".pending--pop-up").classList.remove("hide");
  document.querySelector(".black").classList.remove("hide");

})
const displayCompleted = document.querySelector(".pop-comps");
displayCompleted.addEventListener("click" , function(){
  genratecomPleted(finished);
  document.querySelector(".pending--pop-up").classList.remove("hide");
  document.querySelector(".black").classList.remove("hide");
})
const hidePending = document.querySelector(".display");
hidePending.addEventListener("click",function(){
  document.querySelector(".pending--pop-up").classList.add("hide");
  document.querySelector(".black").classList.add("hide");
})

